import { parse } from "std/encoding/toml.ts"

export default async function toml(path: string | URL) {
  const content = await Deno.readTextFile(path)
  return parse(content)
}
