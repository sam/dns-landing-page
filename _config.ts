import lume from "lume/mod.ts"

// Stable plugins
import attributes from "lume/plugins/attributes.ts"
import codeHighlight from "lume/plugins/code_highlight.ts"
import esbuild from "lume/plugins/esbuild.ts"
import jsx from "lume/plugins/jsx_preact.ts"
import katex from "lume/plugins/katex.ts"
import lightningcss from "lume/plugins/lightningcss.ts"
import metas from "lume/plugins/metas.ts"
import minifyHTML from "lume/plugins/minify_html.ts"
import mdx from "lume/plugins/mdx.ts"
import pug from "lume/plugins/pug.ts"
import remark from "lume/plugins/remark.ts"
import sass from "lume/plugins/sass.ts"
import sitemap from "lume/plugins/sitemap.ts"
import sourceMaps from "lume/plugins/source_maps.ts"
import svgo from "lume/plugins/svgo.ts"

// Experimental plugins

// Custom plugins
import toml from "./custom/toml/toml.ts"

const site = lume({
  src: "./src",
  dest: "./dist",
  location: new URL("https://dns.froth.zone"),
})

site
  .copy("static", ".")
  .copy("static/.well-known", ".well-known")
  .copy(".domains")
  .loadData([".toml"], toml)
  .use(attributes())
  .use(codeHighlight())
  .use(katex())
  .use(metas())
  .use(jsx())
  .use(mdx())
  .use(remark())
  .use(pug())
  .use(sitemap())
  .use(svgo())
  .remoteFile(
    "_includes/styles/external/nord.min.css",
    "https://cdn.jsdelivr.net/gh/highlightjs/cdn-release@11.6.0/build/styles/nord.min.css",
  )
  .use(esbuild({
    extensions: [".ts", ".js"],
  }))
  .use(lightningcss())
  .use(sass())
  .use(minifyHTML())
  .use(sourceMaps({
    sourceContent: true,
  }))

export default site
